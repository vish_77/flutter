main() {
  int x = 10;
  num y = 20.5;
  double z = 30.5;

  print(x);
  print(y);
  print(z);

//x = 30.2;			//Error: A value of type 'double' can't be assigned to a variable of type 'int'.
  y = 15;
  z = 18;

  print(x.runtimeType); 	//int
  print(y.runtimeType);	 	//int
  print(z.runtimeType);		 //double

  y = 40;
  print(y.runtimeType); 	//int

  String name = "Vishal";

  bool flag = true;
  flag = false;

  var a = "More";

//a = 10.3;			//Error: A value of type 'double' can't be assigned to a variable of type 'String'.
//a = true;			//Error: A value of type 'bool' can't be assigned to a variable of type 'String'.

  print(a.runtimeType); 	//String

  dynamic b = 200;
  print(b.runtimeType);		//int

  b = "Vishal";
  print(b.runtimeType);		//String

  b = 20.3;
  print(b.runtimeType);		//double

  b = true;
  print(b.runtimeType);		//bool
}
