int sum(int n) {
  if (n == 1) return 1;

  return n + sum(--n);
}

void main() {
  print(sum(5));
}
