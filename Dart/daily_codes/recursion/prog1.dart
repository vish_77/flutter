void fun(int x) {
  if (x == 0) return;

  print(x);
  fun(--x);
}

void main() {
  fun(5);
}

/*
5
4
3
2
1
*/
