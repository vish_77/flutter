void fun(String name, [double sal = 10.8]){
	
	print(name);
	print(sal);
}

void main(){

	print("In main");
	fun("Vishal");
	fun("Vishal",50.2);
	print("End main");
}

/*
In main
Vishal
10.8
Vishal
50.2
End main
*/
