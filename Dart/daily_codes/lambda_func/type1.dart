var add = (int a, int b) {
  return a + b;
};

void main() {
  print(add(10, 20));		//30
  int x = 10;

  print(x.runtimeType);		//int
  print(add.runtimeType);	//(int, int) => int
}
