
import 'dart:io';

class Employee{
	
	String? name = "Vishal";
	int? id = 1;
	double? sal = 1.0;

	void empInfo(){
		print(name);
		print(id);
		print(sal);
	}
}

void main(){

	
	Employee obj = new Employee();
	obj.empInfo();

	print("Enter empName");
	obj.name = stdin.readLineSync();

	print("Enter Id");
	obj.id = int.parse(stdin.readLineSync()!);

	print("Enter sal");
	obj.sal = double.parse(stdin.readLineSync()!);

	obj.empInfo();
}
