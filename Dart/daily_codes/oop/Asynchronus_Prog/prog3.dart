
Future<String> getOrder(){
	
	return Future.delayed(Duration(seconds:5),()=> "Burger");
}


String YourOrderMessage(){

	var Order = getOrder();
	return "Your order is $Order";
}


void main(){

	print("Start");
	print(YourOrderMessage());
	print("End");
}

/*
Start
Your order is Instance of 'Future<String>'
End
*/
