
import 'dart:io';

String? placeOrder(){
	
	print("Enter value:");
	String? ord = stdin.readLineSync();
	return ord;
}

Future<String?> getOrder() {

	return Future.delayed(Duration(seconds:5),()=> placeOrder());
}

Future<String?> getOrderMessage() async {

	var Order = await getOrder();
	return Order;

}

Future<void> main()async {

	print("Start");
	print(await getOrderMessage());
	print("End");
}

