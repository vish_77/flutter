import 'prog1.dart';

void fun1(){

	for(int i = 0; i<10; i++){

		print("In fun1");
	}
}

void fun2(){

	for(int i = 0; i<10; i++){

		print("In fun2 - for1");
	}
	
	Future.delayed(Duration(seconds:4),()=>print(getName()));
	
	for(int i = 0; i<10; i++){

		print("In fun2 - for2");
	}
}

void main(){

	print("Start main");
	
	Future.delayed(Duration(seconds:2),()=> fun2());
	Future.delayed(Duration(seconds:5),()=> fun1());
	
	print("End main");
}
	
	
	
	
	
